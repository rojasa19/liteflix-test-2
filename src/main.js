import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

new Vue({
  data () {
    return {
      serverUrl: process.env.VUE_APP_SERVER,
      urlImagesOriginal: "https://image.tmdb.org/t/p/original",
      urlImagesResize: "https://image.tmdb.org/t/p/w300"
    }
  },
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
