import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    modalNewMovie: false
  },
  mutations: {
    setModalNewMovie(state, payload) {
      state.modalNewMovie = payload;
    },
  },
  getters: {
    getModalNewMovie: state => state.modalNewMovie
  },
  modules: {
  }
})
