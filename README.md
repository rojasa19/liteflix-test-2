# liteflix-test-2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Link a la web del test
Web [liteflix](https://liteflix.s3-us-west-2.amazonaws.com).
